//
//  LoginLoginViewController.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, ModuleTransitionable {

    // MARK: - IBOutlets
    
    @IBOutlet weak var usernameTextFiel: UITextField!
    @IBOutlet weak var passwordTextFied: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    // MARK: - Public Properties
    
    var output: LoginViewOutput!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        output.viewIsReady()
    }

    // MARK: - IBAction
    
    @IBAction func touchLoginButton(_ sender: Any) {
        output.touchLoginButton()
    }
    
    // MARK: LoginViewInput
    
    func setupInitialState() {
    }
    
}

// MARK: - LoginViewInput

extension LoginViewController: LoginViewInput {
    func getCredentials(completion: (String?, String?) -> Void) {
        completion(usernameTextFiel.text, passwordTextFied.text)
    }
}
