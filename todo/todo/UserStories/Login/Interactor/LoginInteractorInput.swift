//
//  LoginLoginInteractorInput.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import Foundation

struct Credintals {
    var login: String
    var password: String
}

protocol LoginInteractorInput {
    func authentication(credintals: Credintals, completion: (Bool) -> Void)
}
