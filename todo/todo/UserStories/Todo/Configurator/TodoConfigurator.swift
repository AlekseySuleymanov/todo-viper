//
//  TodoTodoConfigurator.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TodoModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? TodoViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: TodoViewController) {

        let router = TodoRouter()

        let presenter = TodoPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TodoInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
