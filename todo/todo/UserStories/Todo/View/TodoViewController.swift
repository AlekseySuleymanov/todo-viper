//
//  TodoTodoViewController.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TodoViewController: UIViewController, TodoViewInput {

    var output: TodoViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: TodoViewInput
    func setupInitialState() {
    }
}
