//
//  TodoTodoViewOutput.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol TodoViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
}
