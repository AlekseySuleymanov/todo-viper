//
//  TodoTodoViewInput.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol TodoViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
}
