//
//  TodoTodoPresenter.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class TodoPresenter: TodoModuleInput, TodoViewOutput, TodoInteractorOutput {

    weak var view: TodoViewInput!
    var interactor: TodoInteractorInput!
    var router: TodoRouterInput!

    func viewIsReady() {

    }
}
