//
//  TaskListServiceRealm.swift
//  todo
//
//  Created by Алексей Сулейманов on 26.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//
import RealmSwift
import Foundation

class TaskListServiceRealm: TaskListServiceProtocol {
    
    static var shared = TaskListServiceRealm()
    private init() {}
    
    let tasksRealm = try! Realm()
    var tasksObject: Results<TaskObject>! {
        return tasksRealm.objects(TaskObject.self)
    }
    
    
    func getTaskList(completion: ([Task]) -> Void) {
        
        var tasks: [Task] = []
        
        tasksObject.forEach { (object) in
            let task = Task(name: object.name,
                            id: object.id,
                            isSolved: object.isSolved,
                            date: object.date,
                            image: UIImage(data: object.image ?? Data()),
                            description: object.descrip)
            tasks.append(task)
        }
        completion(tasks)
    }
    
    func isSolvedOn(taskId: String, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.isSolved = true
            }
            completion(true)}
        catch {
            print(error)
            completion(false)
        }
    }
    
    func isSolvedOff(taskId: String, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.isSolved = false
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
        
    }
    
    func setTaskName(taskId: String, newValue: String, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.name = newValue
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
    }
    
    func setTaskDescription(taskId: String, newValue: String, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.descrip = newValue
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
    }
    
    func addNewTask(completion: (Task) -> Void) {
        let newTask = Task(name: "NewTask")
        do {
            try tasksRealm.write {
                let newTaskObject = TaskObject()
                newTaskObject.name = newTask.name
                newTaskObject.id = newTask.id
                newTaskObject.date = Date()
                tasksRealm.add(newTaskObject)
            }
            completion(newTask)
        }
        catch {
            print(error)
        }
    }
    
    func deleteTask(task: Task, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                guard let objectsToDelete = (tasksObject.first { $0.id == task.id }) else { return }
                tasksRealm.delete(objectsToDelete)
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
    }
    
    func setTaskImage(taskId: String, newValue: UIImage, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.image = newValue.pngData()
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
    }
    
    func removeTaskImage(taskId: String, completion: (Bool) -> Void) {
        do {
            try tasksRealm.write {
                tasksObject.first { $0.id == taskId }?.image = nil
            }
            completion(true)
        }
        catch {
            print(error)
            completion(false)
        }
    }
}
