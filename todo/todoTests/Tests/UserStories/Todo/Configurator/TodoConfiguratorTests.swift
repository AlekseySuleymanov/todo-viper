//
//  TodoTodoConfiguratorTests.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class TodoModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TodoViewControllerMock()
        let configurator = TodoModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TodoViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TodoPresenter, "output is not TodoPresenter")

        let presenter: TodoPresenter = viewController.output as! TodoPresenter
        XCTAssertNotNil(presenter.view, "view in TodoPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TodoPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TodoRouter, "router is not TodoRouter")

        let interactor: TodoInteractor = presenter.interactor as! TodoInteractor
        XCTAssertNotNil(interactor.output, "output in TodoInteractor is nil after configuration")
    }

    class TodoViewControllerMock: TodoViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
