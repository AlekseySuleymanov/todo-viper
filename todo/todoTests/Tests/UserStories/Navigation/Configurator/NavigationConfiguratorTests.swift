//
//  NavigationNavigationConfiguratorTests.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class NavigationModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = NavigationViewControllerMock()
        let configurator = NavigationModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "NavigationViewController is nil after configuration")
        XCTAssertTrue(viewController.output is NavigationPresenter, "output is not NavigationPresenter")

        let presenter: NavigationPresenter = viewController.output as! NavigationPresenter
        XCTAssertNotNil(presenter.view, "view in NavigationPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in NavigationPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is NavigationRouter, "router is not NavigationRouter")

        let interactor: NavigationInteractor = presenter.interactor as! NavigationInteractor
        XCTAssertNotNil(interactor.output, "output in NavigationInteractor is nil after configuration")
    }

    class NavigationViewControllerMock: NavigationViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
