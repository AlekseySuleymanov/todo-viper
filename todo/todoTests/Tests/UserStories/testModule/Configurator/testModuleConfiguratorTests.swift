//
//  testModuletestModuleConfiguratorTests.swift
//  todo
//
//  Created by // on 22/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class testModuleModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = testModuleViewControllerMock()
        let configurator = testModuleModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "testModuleViewController is nil after configuration")
        XCTAssertTrue(viewController.output is testModulePresenter, "output is not testModulePresenter")

        let presenter: testModulePresenter = viewController.output as! testModulePresenter
        XCTAssertNotNil(presenter.view, "view in testModulePresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in testModulePresenter is nil after configuration")
        XCTAssertTrue(presenter.router is testModuleRouter, "router is not testModuleRouter")

        let interactor: testModuleInteractor = presenter.interactor as! testModuleInteractor
        XCTAssertNotNil(interactor.output, "output in testModuleInteractor is nil after configuration")
    }

    class testModuleViewControllerMock: testModuleViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
