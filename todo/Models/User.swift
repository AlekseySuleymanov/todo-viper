//
//  User.swift
//  todo
//
//  Created by Алексей Сулейманов on 25.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//

import Foundation

class User {
    
    // MARK: - Public Properties
    
    var login: String
    var password: String
    
    // MARK: - Init Methods
    
    init(login: String, password: String) {
        self.login = login
        self.password = password
    }
    
}
