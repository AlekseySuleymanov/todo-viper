//
//  Task.swift
//  todo
//
//  Created by Алексей Сулейманов on 23.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//

import UIKit
import Foundation
class Task {
    
    // MARK: - Public Properties
    
    var name: String
    var isSolved: Bool
    var id :String
    var date: Date?
    var image: UIImage?
    var description: String?
    
    // MARK: - Init Methods
    
    init (name: String,
          id: String = UUID().uuidString,
          isSolved: Bool = false,
          date: Date? = nil,
          image: UIImage? = nil,
          description: String? = nil) {
        self.name = name
        self.id = id
        self.isSolved = isSolved
        self.date = date
        self.image = image
        self.description = description
    }
}
