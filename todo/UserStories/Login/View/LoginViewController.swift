//
//  LoginLoginViewController.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, ModuleTransitionable {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    // MARK: - Private Properties
    
    var output: LoginViewOutput!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override func viewDidAppear(_ animated: Bool) {
    }
    // MARK: - LoginViewInput
    
    func setupInitialState() {
    }
    
    // MARK: - IBActions
    
    @IBAction func touchLoginButton(_ sender: Any) {
        output.touchLogin()
    }
    
}

// MARK: - LoginViewInput

extension LoginViewController: LoginViewInput {
    func getCredentials(completion: (String, String) -> Void) {
        guard
            let username = usernameTextField.text,
            let password = passwordTextField.text
            else { return }
        completion(username, password)
    }
}
