//
//  LoginLoginInteractorInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import Foundation

protocol LoginInteractorInput {
    func authentication(credintals: Credentials, completion: (Bool) -> Void)
}

extension LoginInteractorInput {
    func authentication(credintals: Credentials, completion: (Bool) -> Void) {
        completion(true)
    }
}
