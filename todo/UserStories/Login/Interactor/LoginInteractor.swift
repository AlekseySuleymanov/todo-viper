//
//  LoginLoginInteractor.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class LoginInteractor: LoginInteractorInput {
    
    weak var output: LoginInteractorOutput!
    var loginService: LoginServiceProtocol?

    // MARK: - LoginInteractorInput
    
    func authentication(credintals: Credentials, completion: (Bool) -> Void) {
        loginService?.authentication(credintals: credintals, completion: { (answer) in
            completion(answer)
        })
    }
}
