//
//  LoginLoginInteractorOutput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import Foundation

struct Credentials {
    var username: String
    var password: String
}

protocol LoginInteractorOutput: class {

}
