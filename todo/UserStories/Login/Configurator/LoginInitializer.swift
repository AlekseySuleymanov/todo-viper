//
//  LoginLoginInitializer.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class LoginModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var loginViewController: LoginViewController!

    override func awakeFromNib() {

        let configurator = LoginModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: loginViewController)
    }

}
