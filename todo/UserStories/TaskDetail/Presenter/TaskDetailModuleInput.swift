//
//  TaskDetailTaskDetailModuleInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol TaskDetailModuleInput: class {
    func removeImage()
}
