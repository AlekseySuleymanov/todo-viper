//
//  TaskDetailTaskDetailPresenter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

class TaskDetailPresenter: TaskDetailInteractorOutput {
    
    // MARK: Public Properties
    
    weak var view: TaskDetailViewInput!
    weak var presenter: TaskDetailModuleInput!
    var interactor: TaskDetailInteractorInput!
    var router: TaskDetailRouterInput!

    // MARK: Private Properties
    
    private var task: Task
    
    // MARK: Init Methods
    
    init(task: Task) {
        self.task = task
    }
}
    
// MARK: - TaskDetailViewOutput

extension TaskDetailPresenter: TaskDetailViewOutput {
 
    func viewIsReady() {
        view.setup(task: task)
    }
    
    func setNameTask(text: String) {
        interactor.setTaskName(task: task, newValue: text) { (answer) in
            guard answer else { return }
            task.name = text
        }
    }
    
    func setDecriptionTask(text: String) {
        interactor.setTaskDescription(task: task, newValue: text) { (answer) in
            guard answer else { return }
            task.description = text
        }
    }
    
    func openMenu() {
        router.showImageMenu(task: task)
    }
    
    func setImage(image: UIImage) {
        interactor.setTaskImage(task: task, newValue: image) { (answer) in
            guard answer else { return }
            task.image = image
            view.updateTaskImage(image: task.image)
        }
    }
    
    func imagePickerDismiss() {
        router.imagePickerDismiss()
     }
}

extension TaskDetailPresenter: TaskDetailModuleInput {
    
    func removeImage() {
        interactor.removeTaskImage(task: task) { (answer) in
            guard answer else { return }
            task.image = nil
            view.updateTaskImage(image: task.image)
        }
    }
}
