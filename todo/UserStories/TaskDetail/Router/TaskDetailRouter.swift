//
//  TaskDetailTaskDetailRouter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TaskDetailRouter: TaskDetailRouterInput {
    
    // MARK: - Public Properties

    weak var view: ModuleTransitionable?
    
    // MARK: - Private Properties
    
    private var imagePicker:  UIImagePickerController!
    
    // MARK: - TaskDetailRouterInput
    
    func showImageMenu(task: Task) {
        let imageMenuViewController = ImageMenuModuleConfigurator().configure(with: task, output: self)
        view?.presentModule(imageMenuViewController, animated: false, completion: nil)
    }
    
    func showImagePicker() {
        imagePicker = UIImagePickerController()
        imagePicker?.delegate = view  as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker?.sourceType = .photoLibrary
        
        view?.presentModule(imagePicker, animated: true, completion: nil)
    }
    func imagePickerDismiss() {
        imagePicker?.dismiss(animated: false, completion: nil)
    }
}

// MARK: - ImageMenuModuleOutput

extension TaskDetailRouter: ImageMenuModuleOutput {
    func showPage() {
        let tabController = TabsModuleConfigurator().configure()
        view?.push(module: tabController, animated: false)
    }
    
    func changeImage() {
        self.showImagePicker()
    }
    
    func removeImage() {
        let viewController = view as? TaskDetailViewController
        let presenter = viewController?.output as? TaskDetailModuleInput
        presenter?.removeImage()
    }
}
