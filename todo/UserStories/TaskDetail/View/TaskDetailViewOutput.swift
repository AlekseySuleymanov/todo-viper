//
//  TaskDetailTaskDetailViewOutput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol TaskDetailViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
    func setNameTask(text: String)
    func setDecriptionTask(text: String)
    func openMenu()
    func setImage(image: UIImage)
    func imagePickerDismiss()
}
