//
//  TaskDetailTaskDetailViewController.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TaskDetailViewController: UIViewController, ModuleTransitionable {
    
    // MARK: - Constants
    
    enum Constants {
        static var navigationItemTitle = "Detail"
        static var rightBarButtonItemTitle = "..."
        static var borderWidth : CGFloat = 1
    }

    // MARK: - IBOutlets
    
    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var taskDecriptionTextView: UITextView!
    @IBOutlet weak var taskImageView: UIImageView!
    @IBOutlet weak var taskDatePicker: UIDatePicker!
    
    // MARK: - Public Properties
    
    var output: TaskDetailViewOutput!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        output.viewIsReady()
    }
    
    // MARK: - IBActions

    @IBAction func editingDidEndTaskNameTextField(_ sender: UITextField) {
        output.setNameTask(text: sender.text ?? "")
    }
    
    // MARK: - Private Methods
    
    private func configureAppearance() {
        taskDecriptionTextView.delegate = self
        taskDecriptionTextView.layer.borderColor = UIColor.gray.cgColor
        taskDecriptionTextView.layer.borderWidth = Constants.borderWidth
        configureNavigationBar()
    }
    
    private func configureNavigationBar() {
        navigationItem.title = Constants.navigationItemTitle
        let buttonItem = UIBarButtonItem(title: Constants.rightBarButtonItemTitle,
                                         style: .plain,
                                         target: self,
                                         action: #selector(openMenu))
        navigationItem.rightBarButtonItem = buttonItem
    }
    
    @objc
    private func openMenu() {
        output.openMenu()
    }
}

// MARK: - TaskDetailViewInput

extension TaskDetailViewController: TaskDetailViewInput {
    func updateTaskImage(image: UIImage?) {
        taskImageView.image = image
        taskImageView.setNeedsDisplay()
    }
    
    func setupInitialState() {
    }
    
    func setup(task: Task) {
        taskNameTextField.text = task.name
        taskDecriptionTextView.text = task.description
        taskImageView.image = task.image
        taskDatePicker.date = task.date ?? Date()
    }
}

// MARK: - UITextViewDelegate

extension TaskDetailViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        output.setDecriptionTask(text: textView.text ?? "")
    }
}

// MARK: - UIImagePickerControllerDelegate

extension TaskDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        output.imagePickerDismiss()
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        guard let resizedImage = selectedImage.resized(toWidth: 200) else { return }
        output.setImage(image: resizedImage)
    }
}
