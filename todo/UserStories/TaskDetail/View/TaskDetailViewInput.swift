//
//  TaskDetailTaskDetailViewInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol TaskDetailViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
    func setup(task: Task)
    func updateTaskImage(image: UIImage?)
}
