//
//  TaskDetailTaskDetailInteractorInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit
import Foundation

protocol TaskDetailInteractorInput {
    func setTaskName(task: Task, newValue: String, completion: (Bool) -> Void)
    func setTaskDescription(task: Task, newValue: String, completion: (Bool) -> Void)
    func setTaskImage(task: Task, newValue: UIImage, completion: (Bool) -> Void)
    func removeTaskImage(task: Task, completion: (Bool) -> Void)
}
