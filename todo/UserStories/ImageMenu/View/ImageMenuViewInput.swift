//
//  ImageMenuImageMenuViewInput.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol ImageMenuViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
}
