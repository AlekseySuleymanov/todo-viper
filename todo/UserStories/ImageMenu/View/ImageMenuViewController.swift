//
//  ImageMenuImageMenuViewController.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class ImageMenuViewController: UIAlertController, ImageMenuViewInput, ModuleTransitionable {
    
    // MARK: - Constants
    
    enum Constats {
        static var showPageActionTitle: String = "Show Page View"
        static var changeImageActionTitle: String = "Change Image"
        static var removeImageActionTitle: String = "Remove Image"
        static var cancelActionTitle: String = "Cancel"
    }
    
    // MARK: - Public Methods
    
    var output: ImageMenuViewOutput!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActions()
        output.viewIsReady()
    }

    // MARK: - Private Methods

    private func setupActions() {
        
        let showPageAction = UIAlertAction(title: Constats.showPageActionTitle, style: .default) { [weak self] _ in
            self?.output?.showPage()
        }
        self.addAction(showPageAction)
        
        let changeImageAction = UIAlertAction(title: Constats.changeImageActionTitle, style: .default) { [weak self] _ in
            self?.output?.changeImage()
        }
        self.addAction(changeImageAction)
        let removeIamgeActon = UIAlertAction(title: Constats.removeImageActionTitle, style: .destructive) { [weak self] _ in
            self?.output?.removeImage()
        }
        self.addAction(removeIamgeActon)
        
        let cancelAction = UIAlertAction(title: Constats.cancelActionTitle , style: .cancel, handler: nil)
        self.addAction(cancelAction)
    }

    // MARK: - ImageMenuViewInput
    
    func setupInitialState() {
    }
}
