//
//  ImageMenuImageMenuViewOutput.swift
//  todo
//
//  Created by // on 25/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol ImageMenuViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
    func changeImage()
    func removeImage()
    func showPage()
}
