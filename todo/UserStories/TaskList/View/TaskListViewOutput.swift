//
//  TaskListTaskListViewOutput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol TaskListViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
    func addNewTask()
}
