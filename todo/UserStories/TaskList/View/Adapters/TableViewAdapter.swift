//
//  TableViewAdapter.swift
//  todo
//
//  Created by Алексей Сулейманов on 23.11.2019.
//  Copyright © 2019 Алексей Сулейманов. All rights reserved.
//
import UIKit
import Foundation

protocol TableViewAdapterInput {
    func set(tableView: UITableView)
    func configure(with items: [Task])
    func set(_ image: UIImage, for indexPath: IndexPath)
    func reloadData()
}

protocol TableViewAdapterOutput {
    func select(item: Task)
    func delete(item: Task)
}

class TableViewAdapter: NSObject {
    
    // MARK: - Constats
    
    private enum Constants {
        static let numberOfSections: Int = 2
        static let heightForHeaderInSection: CGFloat = 30
        static let todoHeader: String = "ToDo"
        static let solvedHeader: String = "Solved"
    }
    
    // MARK: - Private Properties
    
    private let output: TableViewAdapterOutput?
    private var items: [Task] = [] {
        didSet {
            tableView?.reloadData()
        }
    }
    private (set) var tableView: UITableView? {
        didSet {
            let bundle = Bundle(for: TaskListCell.self)
            tableView?.register(UINib(nibName: TaskListCell.className, bundle: bundle), forCellReuseIdentifier: TaskListCell.className)
        }
    }
    
    // MARK: - Init Methods
    
    init(output: TableViewAdapterOutput?) {
        self.output = output
    }
}

// MARK: - UITableViewDelegate

extension TableViewAdapter: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.heightForHeaderInSection
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return Constants.todoHeader
        case 1:
            return Constants.solvedHeader
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            output?.select(item: items.filter{ $0.isSolved == false }[indexPath.row])
        case 1:
            output?.select(item: items.filter{ $0.isSolved == true }[indexPath.row])
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            switch indexPath.section {
            case 0:
                output?.delete(item: items.filter{ $0.isSolved == false }[indexPath.row])
            case 1:
                output?.delete(item: items.filter{ $0.isSolved == true }[indexPath.row])
            default: break
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension TableViewAdapter: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return items.filter{ $0.isSolved == false }.count
        case 1:
            return items.filter{ $0.isSolved == true }.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TaskListCell.className, for: indexPath)
        if let taskListCell = cell as? TaskListCellInput {
            taskListCell.setup(output: output as? TaskListCellOutput)
            switch indexPath.section {
            case 0:
                taskListCell.setup(task: items.filter{ $0.isSolved == false }[indexPath.row])
            case 1:
                taskListCell.setup(task: items.filter{ $0.isSolved == true }[indexPath.row])
            default: break
            }
        }
        return cell
    }
}

// MARK: - TableViewAdapterInput

extension TableViewAdapter: TableViewAdapterInput {
    func reloadData() {
        tableView?.reloadData()
    }
    
    func set(tableView: UITableView) {
        self.tableView = tableView
    }
    
    func configure(with items: [Task]) {
        self.items = items
    }
    
    func set(_ image: UIImage, for indexPath: IndexPath) {
        
    }
    
}

// MARK: - TaskListCellOutput


