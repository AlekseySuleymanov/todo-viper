//
//  TaskListTaskListViewController.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit



class TaskListViewController: UIViewController, ModuleTransitionable {

    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Constats
    
    enum Constants {
        static var navigationItemTitle = "Task List"
        static var rightBarButtonItemTitle = "Add"
    }
    
    // MARK: - Public Properties
    
    var output: TaskListViewOutput!
    var tableViewAdapter: TableViewAdapterInput!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        configureNavigationBar()
    }

    override func viewDidAppear(_ animated: Bool) {
        output.viewIsReady()
    }
    
    // MARK: - Private Methods
    
    private func configureAppearance() {
        tableViewAdapter = TableViewAdapter(output: output as? TableViewAdapterOutput)
        tableViewAdapter.set(tableView: tableView)
        tableView.delegate = tableViewAdapter as? UITableViewDelegate
        tableView.dataSource = tableViewAdapter as? UITableViewDataSource
    }
    
    private func configureNavigationBar() {
        navigationItem.title = Constants.navigationItemTitle
        let buttonItem = UIBarButtonItem(title: Constants.rightBarButtonItemTitle,
                                         style: .plain,
                                         target: self,
                                         action: #selector(addNewTask))
        navigationItem.rightBarButtonItem = buttonItem
    }
    
    @objc
    private func addNewTask() {
        output.addNewTask()
    }

    // MARK: - TaskListViewInput
    
    func setupInitialState() {
    }
}

// MARK: - TaskListViewInput

extension TaskListViewController: TaskListViewInput {
    func reloadView() {
        tableView.reloadData()
    }
    
    func set(items: [Task]) {
        tableViewAdapter.configure(with: items)
    }
    
    func set(image: UIImage, for indexPath: IndexPath) {
        tableViewAdapter.set(image, for: indexPath)
    }
}
