//
//  TaskListTaskListRouter.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TaskListRouter: TaskListRouterInput {
    
    weak var view: ModuleTransitionable?
    
    func openDetail(task: Task) {
        let taskDetailViewController = TaskDetailModuleConfigurator().configure(with: task)
        view?.push(module: taskDetailViewController, animated: false)
    }
}
