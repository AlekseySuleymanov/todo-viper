//
//  TaskListTaskListRouterInput.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import Foundation

protocol TaskListRouterInput {
    func openDetail(task: Task)
}
