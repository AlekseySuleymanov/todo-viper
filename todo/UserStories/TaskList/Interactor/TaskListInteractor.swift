//
//  TaskListTaskListInteractor.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class TaskListInteractor: TaskListInteractorInput {

    weak var output: TaskListInteractorOutput!
    
    var taskListService: TaskListServiceProtocol?

    // MARK: - TaskListInteractorInput
    
    func getTaskList(completion: ([Task]) -> Void) {
        taskListService?.getTaskList(completion: { (tasks) in
            completion(tasks)
        })
    }
    
    func isSolvedOn(taskId: String, completion: (Bool) -> Void) {
        taskListService?.isSolvedOn(taskId: taskId, completion: { (answer) in
            completion(answer)
        })
    }
    
    func isSolvedOff(taskId: String, completion: (Bool) -> Void) {
        taskListService?.isSolvedOff(taskId: taskId, completion: { (answer) in
            completion(answer)
        })
    }
    
    func addNewTask(completion: (Task) -> Void) {
        taskListService?.addNewTask(completion: { (task) in
            completion(task)
        })
    }
    
    func deleteTask(task: Task, completion: (Bool) -> Void) {
        taskListService?.deleteTask(task: task, completion: { (answer) in
            completion(answer)
        })
     }
}
