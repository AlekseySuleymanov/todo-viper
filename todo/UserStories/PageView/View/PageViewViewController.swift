//
//  PageViewPageViewViewController.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class PageViewViewController: UIPageViewController, ModuleTransitionable {

    // MARK: - Public Properties

    var output: PageViewViewOutput!
    
    // MARK: - Private Properties
    
    private var pages: [UIViewController] = []

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.hidesBottomBarWhenPushed = false
        output.viewIsReady()
    }
}

// MARK: PageViewViewInput

extension PageViewViewController: PageViewViewInput {

    func setupInitialState(pages: [UIViewController]) {
        self.pages = pages
        if let firstPage = pages.first {
            setViewControllers([firstPage], direction: .forward, animated: false, completion: nil)
        }
    }
}

// MARK: - UIPageViewControllerDataSource

extension PageViewViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let previosIndex = viewControllerIndex - 1
        guard previosIndex >= 0 else { return pages.last }
        guard pages.count > previosIndex else { return nil }
        return pages[previosIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count else { return pages.first}
        guard pages.count > nextIndex else { return nil }
        return pages[nextIndex]
    }
}

// MARK: UIPageViewControllerDelegate

extension PageViewViewController: UIPageViewControllerDelegate {
    
}
