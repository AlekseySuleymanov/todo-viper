//
//  PageViewPageViewViewInput.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//
import UIKit

protocol PageViewViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState(pages: [UIViewController])
}
