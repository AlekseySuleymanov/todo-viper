//
//  PageViewPageViewConfigurator.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class PageViewModuleConfigurator {

    func configure() -> PageViewViewController {
       let bundle = Bundle(for: PageViewViewController.self)
       guard let viewController = UIStoryboard(name: String(describing: PageViewViewController.self),
                                     bundle: bundle).instantiateInitialViewController() as? PageViewViewController else {
           fatalError("Can't load PageViewViewController from storyboard, check that controller is initial view controller")
       }

        let router = PageViewRouter()
        router.view = viewController

        let presenter = PageViewPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = PageViewInteractor()
        interactor.output = presenter
        interactor.taskListService = TaskListServiceRealm.shared

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController
    }

}
