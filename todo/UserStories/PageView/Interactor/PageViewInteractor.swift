//
//  PageViewPageViewInteractor.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class PageViewInteractor: PageViewInteractorInput {

    // MARK: - Public Properties
    
    weak var output: PageViewInteractorOutput!
    var taskListService: TaskListServiceProtocol?

    // MARK: - PageViewInteractorInput
    
    func getTaskList(completion: ([Task]) -> Void) {
        taskListService?.getTaskList(completion: { (tasks) in
            completion(tasks)
        })
    }
}
