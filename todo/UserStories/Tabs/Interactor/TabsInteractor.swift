//
//  TabsTabsInteractor.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class TabsInteractor: TabsInteractorInput {


    // MARK: - Public Properties
    
    weak var output: TabsInteractorOutput!
    var taskListService: TaskListServiceProtocol?

    // MARK: - TabsInteractorInput
    
    func getTaskList(completion: ([Task]) -> Void) {
        taskListService?.getTaskList(completion: { (tasks) in
        completion(tasks)
        })
    }
}
