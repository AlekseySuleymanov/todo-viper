//
//  TabsTabsViewOutput.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//


protocol TabsViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
    func tabBarDidSelect(for tag: Int )
}
