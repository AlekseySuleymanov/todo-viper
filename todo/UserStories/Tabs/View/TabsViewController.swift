//
//  TabsTabsViewController.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class TabsViewController: UIViewController, ModuleTransitionable {

    // MARK: - Public Properties

    var output: TabsViewOutput!

    // MARK: - IBOutlets
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
}

// MARK: TabsViewInput

extension TabsViewController: TabsViewInput {
    
    func showView(for tag: Int) {
    }
    
    func set(viewController: UIViewController) {
        
        
        if (self.children.first { $0.view.tag == viewController.view.tag }) == nil {
            addChild(viewController)
            containerView.insertSubview(viewController.view, at: viewController.view.tag)
            viewController.didMove(toParent: self)
        }
        self.children.forEach { $0.view.isHidden = true }
        containerView.viewWithTag(viewController.view.tag)?.isHidden = false
    }
    
    func setupInitialState(items: [UITabBarItem]) {
        tabBar.setItems(items, animated: false)
        tabBar.delegate = self
        output.tabBarDidSelect(for: 0)
    }
}

// MARK: - UITabBarDelegate

extension TabsViewController: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        output.tabBarDidSelect(for: item.tag)
    }
}
