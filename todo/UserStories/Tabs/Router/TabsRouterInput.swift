//
//  TabsTabsRouterInput.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import Foundation

protocol TabsRouterInput {
    func showView(for task: Task)
}
