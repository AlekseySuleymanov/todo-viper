//
//  testModuletestModuleViewController.swift
//  todo
//
//  Created by // on 03/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import UIKit

class testModuleViewController: UIViewController, ModuleTransitionable {

    // MARK: - Public Properties

    var output: testModuleViewOutput!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
}

// MARK: testModuleViewInput

extension testModuleViewController: testModuleViewInput {

        func setupInitialState() {
        }
        
    }
