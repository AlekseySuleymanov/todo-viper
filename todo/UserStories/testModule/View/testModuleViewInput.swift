//
//  testModuletestModuleViewInput.swift
//  todo
//
//  Created by // on 03/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol testModuleViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
}
