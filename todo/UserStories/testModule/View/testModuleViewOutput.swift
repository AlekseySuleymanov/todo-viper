//
//  testModuletestModuleViewOutput.swift
//  todo
//
//  Created by // on 03/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol testModuleViewOutput {

    /**
        @author //
        Notify presenter that view is ready
    */

    func viewIsReady()
}
