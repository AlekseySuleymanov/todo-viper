//
//  testModuletestModulePresenter.swift
//  todo
//
//  Created by // on 03/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

class testModulePresenter: testModuleInteractorOutput {

    // MARK: Public Properties
    
    weak var view: testModuleViewInput!
    var interactor: testModuleInteractorInput!
    var router: testModuleRouterInput!

}

// MARK: - testModuleViewOutput

extension testModulePresenter: testModuleViewOutput {

    func viewIsReady() {
    }

}

// MARK: - testModuleModuleInput

extension testModulePresenter: testModuleModuleInput {

}
