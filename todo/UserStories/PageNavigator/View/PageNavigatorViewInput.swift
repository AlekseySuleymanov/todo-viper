//
//  PageNavigatorPageNavigatorViewInput.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

protocol PageNavigatorViewInput: class {

    /**
        @author //
        Setup initial state of the view
    */

    func setupInitialState()
}
