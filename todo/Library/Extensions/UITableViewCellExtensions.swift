//
import UIKit
import Foundation

extension UITableViewCell {
    static var className: String {
        return String(describing: self)
    }
}
