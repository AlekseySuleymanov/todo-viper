//
//  TabsTabsConfiguratorTests.swift
//  todo
//
//  Created by // on 09/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class TabsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TabsViewControllerMock()
        let configurator = TabsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TabsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TabsPresenter, "output is not TabsPresenter")

        let presenter: TabsPresenter = viewController.output as! TabsPresenter
        XCTAssertNotNil(presenter.view, "view in TabsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TabsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TabsRouter, "router is not TabsRouter")

        let interactor: TabsInteractor = presenter.interactor as! TabsInteractor
        XCTAssertNotNil(interactor.output, "output in TabsInteractor is nil after configuration")
    }

    class TabsViewControllerMock: TabsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
