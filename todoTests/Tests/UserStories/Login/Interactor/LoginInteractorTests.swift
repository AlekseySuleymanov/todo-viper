//
//  LoginLoginInteractorTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class LoginInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class MockPresenter: LoginInteractorOutput {
    }
    
    class MockLoginService: LoginServiceProtocol {
        var isComplitioned = false
        func authentication(credintals: Credentials, completion: (Bool) -> Void) {
            isComplitioned = true
            completion(true)
        }
    }
    
    func testAuthentication() {
        
        let loginInteractor = LoginInteractor()
        let loginService = MockLoginService()
        let credintals = Credentials(username: "Foo", password: "Bar")

        loginInteractor.loginService = loginService
        
        loginInteractor.authentication(credintals: credintals) { (answer) in
            XCTAssertTrue(loginService.isComplitioned)
        }
    }
}
