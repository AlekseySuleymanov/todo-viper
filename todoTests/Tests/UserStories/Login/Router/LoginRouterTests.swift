//
//  LoginLoginRouterTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class LoginRouterTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    class MockLoginViewController: LoginViewController {
        
    }
    
    func testShowTodoList() {
        
        let loginRouter = LoginRouter()
        let view = MockLoginViewController()
        let navigationController = UINavigationController(rootViewController: view)
        
        loginRouter.view = view
        loginRouter.showTodoList()
        
        XCTAssertTrue(navigationController.visibleViewController is TaskListViewController)
    }
    
}
