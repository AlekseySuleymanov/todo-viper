//
//  TaskDetailTaskDetailConfiguratorTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class TaskDetailModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TaskDetailModuleConfigurator().configure(with: Task(name: "Foo"))

        //when
        //configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TaskDetailViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TaskDetailPresenter, "output is not TaskDetailPresenter")

        let presenter: TaskDetailPresenter = viewController.output as! TaskDetailPresenter
        XCTAssertNotNil(presenter.view, "view in TaskDetailPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in TaskDetailPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is TaskDetailRouter, "router is not TaskDetailRouter")

        let interactor: TaskDetailInteractor = presenter.interactor as! TaskDetailInteractor
        XCTAssertNotNil(interactor.output, "output in TaskDetailInteractor is nil after configuration")
    }

//    class TaskDetailViewControllerMock: TaskDetailViewController {
//
//        var setupInitialStateDidCall = false
//
//        override func setupInitialState() {
//            setupInitialStateDidCall = true
//        }
//   }
}
