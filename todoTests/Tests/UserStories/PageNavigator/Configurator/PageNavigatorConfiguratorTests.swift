//
//  PageNavigatorPageNavigatorConfiguratorTests.swift
//  todo
//
//  Created by // on 30/12/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest

class PageNavigatorModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = PageNavigatorViewControllerMock()
        let configurator = PageNavigatorModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "PageNavigatorViewController is nil after configuration")
        XCTAssertTrue(viewController.output is PageNavigatorPresenter, "output is not PageNavigatorPresenter")

        let presenter: PageNavigatorPresenter = viewController.output as! PageNavigatorPresenter
        XCTAssertNotNil(presenter.view, "view in PageNavigatorPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in PageNavigatorPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is PageNavigatorRouter, "router is not PageNavigatorRouter")

        let interactor: PageNavigatorInteractor = presenter.interactor as! PageNavigatorInteractor
        XCTAssertNotNil(interactor.output, "output in PageNavigatorInteractor is nil after configuration")
    }

    class PageNavigatorViewControllerMock: PageNavigatorViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
