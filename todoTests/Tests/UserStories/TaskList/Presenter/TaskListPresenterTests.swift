//
//  TaskListTaskListPresenterTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class TaskListPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: TaskListInteractorInput {
        func getTaskList(completion: ([Task]) -> Void) {
            
        }
        
        func isSolvedOn(taskId: String, completion: (Bool) -> Void) {
            
        }
        
        func isSolvedOff(taskId: String, completion: (Bool) -> Void) {
            
        }
        
        func addNewTask(completion: (Task) -> Void) {
            
        }
        
        func deleteTask(task: Task, completion: (Bool) -> Void) {
            
        }
        

    }

    class MockRouter: TaskListRouterInput {
        func openDetail(task: Task) {
            
        }
    }

    class MockViewController: TaskListViewInput {
        func reloadView() {
            
        }
        
        func set(items: [Task]) {
            
        }
        
        func set(image: UIImage, for indexPath: IndexPath) {
            
        }
        
        func setupInitialState() {

        }
    }
}
