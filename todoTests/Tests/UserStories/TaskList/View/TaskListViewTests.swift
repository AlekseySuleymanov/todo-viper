//
//  TaskListTaskListViewTests.swift
//  todo
//
//  Created by // on 23/11/2019.
//  Copyright © 2019 Alexey Suleymanov. All rights reserved.
//

import XCTest
@testable import todo

class TaskListViewTests: XCTestCase {

    let taskListViewController = TaskListViewController()
    let output = MockTaskListPresenter()
    let tableViewAdapter = MockTableViewAdapter()
    
    override func setUp() {
        super.setUp()
        taskListViewController.output = output
        taskListViewController.tableViewAdapter = tableViewAdapter
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockTaskListPresenter: TaskListViewOutput {
        func viewIsReady() {
            
        }
        
        func addNewTask() {
            
        }
    }
    
    class MockTableViewAdapter: TableViewAdapterInput {
        
        public var tasks: [Task] = []
        public var image: UIImage?
        public var indexPath: IndexPath?
        public var isReloaded = false
        
        func set(tableView: UITableView) {
            
        }
        
        func configure(with items: [Task]) {
            tasks = items
        }
        
        func set(_ image: UIImage, for indexPath: IndexPath) {
            self.image = image
            self.indexPath = indexPath
        }
        
        func reloadData() {
            isReloaded = true
        }
        
    }
    
    class MockTableView : UITableView {
        public var isReloaded = false
        override func reloadData() {
            isReloaded = true
        }
    }
    
    func testSetTaskToAdapter() {
        let tasks = [ Task(name: "Foo"),
                      Task(name: "Bar") ]
        
        taskListViewController.set(items: tasks)
        
        XCTAssertEqual(tableViewAdapter.tasks[0].id, tasks[0].id)
        XCTAssertEqual(tableViewAdapter.tasks[1].id, tasks[1].id)
    }
    
    func testSetImageToAdapter() {
        
        let image = UIImage(systemName: "test")
        let indexPath = IndexPath(item: 0, section: 0)
        
        //taskListViewController.set(image: image!, for: indexPath)
        
        XCTAssertEqual(tableViewAdapter.image, image)
        XCTAssertEqual(tableViewAdapter.indexPath, indexPath)
    }
    
    func testReloadView() {
        
        let tableView = MockTableView()
        taskListViewController.tableView = tableView
        taskListViewController.reloadView()
        
        XCTAssertTrue(tableView.isReloaded)
    }
}
